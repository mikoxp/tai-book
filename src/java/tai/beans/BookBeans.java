package tai.beans;

import java.io.Serializable;

/**
 *
 * @author mik
 * @version 1.1
 */

public class BookBeans implements Serializable {

    private String title;
    private String author;
    private String year;
    private String description;
    private String photoLink;

    public BookBeans() {
    }

    /**
     *
     * @param title tytuł książki
     * @param author autor
     * @param year czas powstania
     * @param description opis
     * @param photoLink link do obrazka
     */
    public BookBeans(String title, String author, String year, String description, String photoLink) {
        this.title = title;
        this.author = author;
        this.year = year;
        this.description = description;
        this.photoLink = photoLink;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoLink() {
        return photoLink;
    }

    public void setPhotoLink(String link) {
        this.photoLink = link;
    }

}
