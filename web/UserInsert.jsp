<%-- 
    Document   : UserInsert
    Created on : 2015-03-25, 17:32:54
    Author     : mik
--%>

<% request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
%>
<jsp:useBean id="sql" scope="session" class="tai.beans.UseSqlLiteDataBaseBeans" />
<jsp:useBean id="user" scope="session" class="tai.beans.UserBeans" />
<jsp:setProperty name="user" property="*"  />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dodanie Użytkownika</title>
    </head>
    <body>
        <%if (sql.insertUser(user.getLogin(), user.getPassword(), user.getName(), user.getSurname(), "user")) {
                out.println("Pomyślnie dodano użytkownika");
        %>
        <p><A HREF="LogIn.jsp">Powrót do palelu logowania</A></p>
                <%
                } else {
                    out.println("Dany login jest zajęty");
                %>
        <p><A HREF="UserForm.jsp">Powrót</A></p>
                <%
                    }
                %>

    </body>
</html>
