<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%-- 
    Document   : BookForm
    Created on : 2015-03-21, 15:44:11
    Author     : mik
--%>

<jsp:include page="Function.jsp" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formularz nowej książki</title>
    </head>
    <body>
        <jsp:include page="LogStatus.jsp" />
        <form action="BookInsert.jsp" method="POST" onsubmit="if (sprawdz(this))
                    return true;
                return false">
            <table border="1">
                <thead>
                    <tr>
                        <th>FORMULARZ</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Nazwa</td>
                        <td><input type="text" name="title" value="" maxlength="20" /></td>
                    </tr>
                    <tr>
                        <td>Autor</td>
                        <td><input type="text" name="author" value="" maxlength="20" /></td>
                    </tr>
                    <tr>
                        <td>Czas powstania</td>
                        <td><input type="text" name="year" value="" maxlength="20" /></td>
                    </tr>
                    <tr>
                        <td>Opis</td>
                        <td><textarea name="description" cols="21" rows="10" maxlength="70" ></textarea></td>
                    </tr>
                    <tr>
                        <td>Link do okładki</td>
                        <td><input type="url" name="photoLink" value="" maxlength="100" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="Dodaj" /></td>
                    </tr>
                </tbody>
            </table>

        </form>
        <p><A HREF="Panel.jsp">Powrót do Menu głównego</A></p> 
    </body>
</html>
