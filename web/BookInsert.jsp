<%-- 
    Document   : BookToBeans
    Created on : 2015-03-21, 15:46:33
    Author     : mik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<jsp:useBean id="sql" scope="session" class="tai.beans.UseSqlLiteDataBaseBeans" />
<jsp:useBean id="book" scope="session" class="tai.beans.BookBeans" />
<jsp:setProperty name="book" property="*"  />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formularz nowej książki cd</title>
    </head>
    <body>
        <jsp:include page="LogStatus.jsp" />
        <p> 
            <%  if(session.getAttribute("user")==null)
                    {
                      %><jsp:forward page="LogIn.jsp" /><%  
                    }
            else{
                if (sql.insertBook(book.getTitle(), book.getAuthor(), book.getYear(), book.getDescription(), book.getPhotoLink())) {
                    out.println("Pomyślnie dodano książkę");
                } else {
                    out.println("Książka już istnieje w bazie");
                }
            }
            %></p>
        <p><A HREF="Panel.jsp">Powrót</A></p>
    </body>
</html>
