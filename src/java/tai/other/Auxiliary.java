package tai.other;

import tai.beans.UseSqlLiteDataBaseBeans;

public class Auxiliary {
/**
 * 
 * @param login login
 * @return Odblokój/Blokój
 */
    public String logInscription(String login) {
        UseSqlLiteDataBaseBeans sql = new UseSqlLiteDataBaseBeans();
        String inscription = "Odblokój";
        if (sql.userPermitLoged(login)) {
            inscription = "Blokój";
        }

        return inscription;
    }
}
