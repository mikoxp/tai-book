<%-- 
    Document   : Panel
    Created on : 2015-03-27, 18:16:51
    Author     : mik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Panel Główny</title>
    </head>
    <body>
        <jsp:include page="LogStatus.jsp" />

        <h1>Menu Główne</h1>

        <form action="BooksList.jsp">

            <input type="hidden" name="choose" value="" disabled="disabled" />
            <input type="hidden" name="phase" value="" disabled="disabled" />
            <input type="submit" value="Lista książek" />
        </form>
        <form action="BookForm.jsp"><input type="submit" value="Dodaj Książke" />
        </form>

    </body>
</html>
