/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tai.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import tai.beans.UseSqlLiteDataBaseBeans;
import tai.beans.UserBeans;

/**
 *
 * @author mik
 */
@WebServlet(name = "VerificationServlet", urlPatterns = {"/VerificationServlet.do"})
public class VerificationServlet extends HttpServlet {

    private UseSqlLiteDataBaseBeans sql;
    private UserBeans user;
    private HttpSession session;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        session = request.getSession();

        sql = new UseSqlLiteDataBaseBeans();
        user = sql.logVerification(request.getParameter("login"), request.getParameter("password"));
        try (PrintWriter out = response.getWriter()) {

            if (user == null) {
                session.setAttribute("info", "Nie udane logowanie");
                request.getRequestDispatcher("LogFailed.jsp").forward(request, response);
            } else {
                
                if (!sql.userPermitLoged(user.getLogin())) {
                    session.setAttribute("info", "Konto zostało zablokowane");
                    request.getRequestDispatcher("LogFailed.jsp").forward(request, response);
                }
                
                session.setMaxInactiveInterval(5 * 60);
                
                session.setAttribute("user", user);
                request.getRequestDispatcher("Panel.jsp").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
