<%-- 
    Document   : Profile
    Created on : 2015-03-29, 17:37:23
    Author     : mik
--%>

<%@page import="tai.beans.UserBeans"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%  UserBeans user = (UserBeans) session.getAttribute("user");
    if (user == null) {
%>
<jsp:forward page="LogIn.jsp" />
<%
    }
%>



<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Profil Użytkownika : <%=user.getLogin()%></title>

    </head>
    <body>
        <jsp:include page="Function.jsp" />
        <jsp:include page="LogStatus.jsp" />

        <h1>Profil Użytkownika <%=user.getLogin()%></h1>
        <p></p>

        <form action="UpdateUser.do" onsubmit="if (haslo(this, '<%=user.getPassword()%>'))
                    return true;
                return false">
            <table border="1">
                <thead>
                    <tr>
                        <th>ZMIEN HASŁO</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Aktualne Hasło</td>
                        <td><input type="password" name="oldPassword" value="" /></td>
                    </tr>
                    <tr>
                        <td>Nowe Hasło</td>
                        <td><input type="password" name="password" value="" /></td>
                    </tr>
                    <tr>
                        <td>Powtórz Hasło</td>
                        <td><input type="password" name="rPassword" value="" /></td>
                    </tr>
                    <tr>
                        <td><input type="hidden" name="akcja" value="haslo" />
                            <input type="hidden" name="login" value="<%=user.getLogin()%>" />
                            <input type="reset" value="RESET" /></td>
                        <td><input type="submit" value="ZMIEŃ" /></td>
                    </tr>
                </tbody>
            </table>
        </form>
        <h1></h1>
        <form action="UpdateUser.do" onsubmit="if (sprawdz(this)) {
                    alert('W celu utrwalenia danych użytkownik zostanie WYLOGOWANY !!!!');
                    return true;
                }
                return false">
            <table border="1">
                <thead>
                    <tr>
                        <th>Personalia</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Imię</td>
                        <td><input type="text" name="name" value="<%=user.getName()%>" /></td>
                    </tr>
                    <tr>
                        <td>Nazwisko</td>
                        <td><input type="text" name="surname" value="<%=user.getSurname()%>" /></td>
                    </tr>
                    <tr>
                        <td><input type="hidden" name="akcja" value="person" />
                            <input type="hidden" name="login" value="<%=user.getLogin()%>" />
                            <input type="reset" value="Reset" /></td>
                        <td><input type="submit" value="Zmień" /></td>
                    </tr>
                </tbody>
            </table>

        </form>
    </body>
</html>
