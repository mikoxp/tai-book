<%-- 
    Document   : BookDetails
    Created on : 2015-03-24, 17:11:51
    Author     : mik
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="sql" scope="session" class="tai.beans.UseSqlLiteDataBaseBeans" />
<jsp:useBean id="book" scope="session" class="tai.beans.BookBeans" />
<!DOCTYPE html>
<%
    book = sql.selectOneBook(request.getParameter("title"));
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%out.println(book.getAuthor() + " " + book.getTitle());%></title>
    </head>
    <body>
        <jsp:include page="LogStatus.jsp" />
        <h2>
            <p><img src="<%=book.getPhotoLink()%>" alt="Zdjecie niedostepne"/>
            </p>
            <p>Tytuł: <%=book.getTitle()%></p>
            <p>Autor: <%=book.getAuthor()%></p>
            <p>Czas powstania: <%=book.getYear()%></p>
            <p>Opis: <%=book.getDescription()%></p>

        </h2>
        <p><A HREF="Panel.jsp">Panel Główny</A></p>
        <p><A HREF="BooksList.jsp">Powrót do listy</A></p>
    </body>
</html>
