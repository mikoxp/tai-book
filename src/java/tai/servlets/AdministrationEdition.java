/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tai.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tai.beans.UseSqlLiteDataBaseBeans;

/**
 *
 * @author mik
 */
@WebServlet(name = "AdminEdition", urlPatterns = {"/AdminEdition.do"})
public class AdministrationEdition extends HttpServlet {

    private UseSqlLiteDataBaseBeans sql;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {
            if (request.getSession().getAttribute("user") == null) {
                request.getRequestDispatcher("LogIn.jsp").forward(request, response);
            } else {
                sql = new UseSqlLiteDataBaseBeans();
                /* TODO output your page here. You may use following sample code. */
                if (request.getParameter("akcja").equals("delete")) {
                    sql.deleteUser(request.getParameter("login"));
                } else {
                    if (request.getParameter("akcja").equals("ban")) {
                        boolean permit = sql.userPermitLoged(request.getParameter("login"));
                        sql.userChangePermitLoged(request.getParameter("login"), !permit);
                    } else {
                        sql.updateUserLevel(request.getParameter("login"), request.getParameter("level"));
                    }
                }

                request.getRequestDispatcher("Administration.jsp").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
