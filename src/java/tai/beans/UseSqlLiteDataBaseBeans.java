package tai.beans;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author mik
 */
public class UseSqlLiteDataBaseBeans implements Serializable {

    private Connection connection = null;
    private Statement stmt = null;
    private PreparedStatement st;

    public UseSqlLiteDataBaseBeans() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:test23.db");
            createTable();

        } catch (ClassNotFoundException | SQLException e) {

        }
    }

    /**
     *
     * @throws SQLException jak dane już powstały
     */
    private void createTable() throws SQLException {

        String tabBook = "CREATE TABLE IF NOT EXISTS books"
                + "(title VARCHAR(20)     NOT NULL,"
                + " author VARCHAR(20)    NOT NULL, "
                + " year   VARCHAR(20) NOT NULL, "
                + " description  VARCHAR(70) NOT NULL, "
                + " photoLink        VARCHAR(100),"
                + "CONSTRAINT pk	PRIMARY KEY(title))";
        String tabUser = "CREATE TABLE IF NOT EXISTS users"
                + "(login VARCHAR(30)     NOT NULL,"
                + " password VARCHAR(30)    NOT NULL, "
                + " name        VARCHAR(30),"
                + " surname       VARCHAR(30),"
                + "level       VARCHAR(10),"
                + "CONSTRAINT pk	PRIMARY KEY(login))";
        String tabPermit = "CREATE TABLE IF NOT EXISTS permit"
                + "(login VARCHAR(30)     NOT NULL,"
                + "log BOOLEAN NOT NULL,"
                + "CONSTRAINT pk	PRIMARY KEY(login),"
                + "CONSTRAINT fk	FOREIGN KEY(login)  REFERENCES users(login))";
        stmt = connection.createStatement();
        stmt.executeUpdate(tabBook);
        stmt.executeUpdate(tabUser);
        stmt.executeUpdate(tabPermit);
        stmt.close();
        insertUser("admin", "admin", "admin", "admin", "admin");

    }

    /**
     *
     * @return czy jest połaczenie
     */
    public boolean isConnection() {
        return connection != null;

    }
//     BOOK

    /**
     *
     * @param title tytuł książki
     * @param author autor
     * @param year czas powstania
     * @param description opis
     * @param photoLink link do obrazka
     * @return czy się udało
     */
    public boolean insertBook(String title, String author, String year, String description, String photoLink) {
        String query = "INSERT INTO books VALUES(?,?,?,?,?);";
        try {
            st = connection.prepareStatement(query);
            st.setString(1, title);
            st.setString(2, author);
            st.setString(3, year);
            st.setString(4, description);
            st.setString(5, photoLink);
            st.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }

    }

    /**
     *
     * @param choose parametr wyszukiwania
     * @param phase fraze wyszukiwana
     * @return liste książek
     */
    public ArrayList<BookBeans> selectBookRecord(String choose, String phase) {
        String query;
        query = "SELECT * FROM books ";
        if (choose != null || !"".equals(choose)) {
            if ("Autor".equals(choose)) {
                query = query + "WHERE lower(author) LIKE '%" + phase + "%'";
            }
            if ("Tytuł".equals(choose)) {
                query = query + "WHERE lower(title) LIKE '%" + phase + "%'";
            }
            if ("Opis".equals(choose)) {
                query = query + "WHERE lower(description) LIKE '%" + phase + "%'";
            }
        }
        query = query + " ORDER BY lower(author);";
        ArrayList<BookBeans> bookList = new ArrayList<>();
        BookBeans book;
        try {
            st = connection.prepareStatement(query);

            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    book = new BookBeans(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                    bookList.add(book);

                }
            }
            st.close();
        } catch (Exception e) {
        }
        return bookList;
    }

    /**
     *
     * @param title tytuł
     * @return dane książki
     */
    public BookBeans selectOneBook(String title) {
        String query = "SELECT * FROM books WHERE title=?;";
        BookBeans book = new BookBeans("pusta", "pusta", "pusta", "pusta", "pusta");

        try {
            st = connection.prepareStatement(query);
            st.setString(1, title);
            try (ResultSet rs = st.executeQuery()) {
                rs.next();
                book = new BookBeans(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
            }
            st.close();
        } catch (Exception e) {
        }
        return book;
    }

    /**
     *
     * @param title tytuł
     * @return czy się udało usunąć
     */
    public boolean deleteBook(String title) {
        String query = "DELETE from books WHERE title=?;";
        try {

            st = connection.prepareStatement(query);
            st.setString(1, title);
            st.executeUpdate();

            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;

        }
    }

//USERS
    /**
     *
     * @param login login
     * @param password haslo
     * @param name imie
     * @param surname nazwisko
     * @param level poziom uprawnien
     * @return czy sie udało
     */
    public boolean insertUser(String login, String password, String name, String surname, String level) {
        String query = "INSERT INTO users VALUES(?,?,?,?,?);";
        String query2 = "INSERT INTO permit VALUES(?,?);";
        try {
            st = connection.prepareStatement(query);
            st.setString(1, login);
            st.setString(2, password);
            st.setString(3, name);
            st.setString(4, surname);
            st.setString(5, level);
            st.executeUpdate();
            st = connection.prepareStatement(query2);
            st.setString(1, login);
            st.setBoolean(2, true);
            st.executeUpdate();
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    /**
     *
     * @return liste użytkowników
     */
    public ArrayList<UserBeans> selectAllOvertUser() {
        String query;
        query = "SELECT * FROM users WHERE login<>'admin';";
        ArrayList<UserBeans> userList = new ArrayList<>();
        UserBeans user;
        try {
            st = connection.prepareStatement(query);
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    user = new UserBeans(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                    userList.add(user);

                }
            }
            st.close();
        } catch (Exception e) {
        }
        return userList;
    }

    /**
     *
     * @param login login
     * @param password hasło
     * @return dane zalogowanego
     */
    public UserBeans logVerification(String login, String password) {

        String query = "SELECT * FROM users WHERE login=? AND password=?;";
        UserBeans user = null;

        try {
            st = connection.prepareStatement(query);
            st.setString(1, login);
            st.setString(2, password);
            try (ResultSet rs = st.executeQuery()) {
                rs.next();
                user = new UserBeans(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
            }
            st.close();
        } catch (Exception e) {
        }
        return user;
    }

    /**
     *
     * @param login login
     * @return czy jest logowalny
     */
    public boolean userPermitLoged(String login) {
        String query = "SELECT log FROM permit WHERE login=?;";
        boolean status = false;
        try {
            st = connection.prepareStatement(query);
            st.setString(1, login);
            ResultSet rs = st.executeQuery();
            status = rs.getBoolean(1);
            rs.close();
            st.close();
        } catch (Exception e) {
        }
        return status;
    }

    /**
     *
     * @param login login
     * @param permit pozwolenie logowania
     */
    public void userChangePermitLoged(String login, boolean permit) {
        String query = "UPDATE permit SET log=?WHERE login=?;";
        try {

            st = connection.prepareStatement(query);
            st.setString(2, login);
            st.setBoolean(1, permit);
            st.executeUpdate();

        } catch (Exception e) {
            System.out.print(e);
        }

    }

    /**
     *
     * @param login login
     * @return czy się udało usunąć
     */
    public boolean deleteUser(String login) {
        System.out.println("deleteUser start");
        //String query = "DELETE from users WHERE login='" + login + "';";
        String query = "DELETE from users WHERE login=?";
        String query2 = "DELETE from permit WHERE login=?";
        try {

            st = connection.prepareStatement(query);
            st.setString(1, login);
            st.executeUpdate();
            st = connection.prepareStatement(query2);
            st.setString(1, login);
            st.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;

        }

    }

    /**
     *
     * @param login login
     * @param password hasło
     * @return czy sie udało
     */
    public boolean updateUserPassword(String login, String password) {
        String query = "UPDATE users SET password='" + password + "'WHERE login='" + login + "';";
        try {
            stmt = connection.createStatement();
            stmt.executeUpdate(query);

            return true;
        } catch (Exception e) {
        }
        return false;

    }

    /**
     *
     * @param login login
     * @param name imie
     * @param surname nazwisko
     * @return sukces
     */
    public boolean updateUserPersonalities(String login, String name, String surname) {
        String query = "UPDATE users SET name='" + name + "', surname='" + surname + "'WHERE login='" + login + "';";
        try {
            stmt = connection.createStatement();
            stmt.executeUpdate(query);

            return true;
        } catch (Exception e) {
        }
        return false;

    }

    /**
     *
     * @param login login
     * @param level nowy poziom uprawnien
     * @return sukces
     */
    public boolean updateUserLevel(String login, String level) {
        String query = "UPDATE users SET level='" + level + "'WHERE login='" + login + "';";
        try {
            stmt = connection.createStatement();
            stmt.executeUpdate(query);

            return true;
        } catch (Exception e) {
        }
        return false;

    }

}
