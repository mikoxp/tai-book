

<%@page import="java.util.ArrayList"%>
<%@page import="tai.beans.UserBeans"%>
<%@page import="tai.other.Auxiliary" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    UserBeans loged = (UserBeans) session.getAttribute("user");
    if (loged==null || !loged.IsAdmin()) {
%>
<jsp:forward page="LogIn.jsp" />
<%
    }
    Auxiliary help = new Auxiliary();

%>
<jsp:useBean id="sql" scope="session" class="tai.beans.UseSqlLiteDataBaseBeans" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Panel Administratora</title>
    </head>
    <body>
        <jsp:include page="LogStatus.jsp" />
        <%  ArrayList<UserBeans> users;
            users = sql.selectAllOvertUser();
        %>
        <table border="1">
            <thead>
                <tr>
                    <th>Login</th>
                    <th>Imię</th>
                    <th>Nazwisko</th>
                    <th>Uprawnienia</th>
                    <th>Usuwanie</th>
                    <th>Zmiana</th>
                    <th>Blokada</th>
                </tr>
            </thead>
            <tbody>
                <%
                    for (UserBeans user : users) {%>
                <tr>
                    <td><%=user.getLogin()%></td>
                    <td><%=user.getName()%></td>
                    <td><%=user.getSurname()%></td>
                    <td><%=user.getLevel()%></td>
                    <% if (!user.getLogin().equals(loged.getLogin())) {%>
                    <td><form action="AdminEdition.do">
                            <input type="hidden" name="akcja" value="delete" />
                            <input type="hidden" name="login" value="<%=user.getLogin()%>" />
                            <input type="submit" value="Usuń" />
                        </form>
                    </td>
                    <td>
                        <form action="ChangeUserLevel.jsp">   
                            <input type="hidden" name="login" value="<%=user.getLogin()%>" />
                            <input type="submit" value="Zmień Uprawnienia" />
                        </form>   
                    </td>
                    <td>
                        <form action="AdminEdition.do">
                            <input type="hidden" name="akcja" value="ban" />  
                            <input type="hidden" name="login" value="<%=user.getLogin()%>" />              
                            <input type="submit" value="<%=help.logInscription(user.getLogin())%>" />
                        </form>
                    </td>
                    <%} else {%>
                    <td></td><td></td><td></td>
                    <%}%>
                </tr>
                <%
                    }
                %>
            </tbody>
        </table>





    </body>
</html>
