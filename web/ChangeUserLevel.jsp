<%-- 
    Document   : CahngeUserLevel
    Created on : 2015-04-09, 19:05:26
    Author     : mik
--%>

<%@page import="tai.beans.UserBeans"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    UserBeans loged = (UserBeans) session.getAttribute("user");;
    if (!"admin".equals(loged.getLevel())) {
%>
<jsp:forward page="LogIn.jsp" />
<%
    }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Change User Level</title>
    </head>
    <body>
        <jsp:include page="LogStatus.jsp" />
        <form action="AdminEdition.do">
            <table border="1">
                <thead>
                    <tr>
                        <th>Wybierz poziom uprawnień</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <select name="level">
                                <option>user</option>
                                <option>admin</option>
                            </select>
                        </td>

                    </tr>
                    <tr>

                        <td>
                            <input type="hidden" name="akcja" value="change" />
                            <input type="hidden" name="login" value="<%=request.getParameter("login")%>" />
                            <input type="submit" value="Zmień" />
                        </td>
                    </tr>
                </tbody>
            </table>

        </form>
    </body>
</html>
