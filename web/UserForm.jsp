<%-- 
    Document   : UserForm
    Created on : 2015-03-25, 17:31:11
    Author     : mik
--%>

<% request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="Function.jsp" />
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formularz nowego Użytkownika</title>
    </head>
    <body>
        <form action="UserInsert.jsp" method="POST" onsubmit="if (rej(this))
                    return true;
                return false">
            <table border="1">
                <thead>
                    <tr>
                        <th>Formularz nowego Użytkownika</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Login</td>
                        <td><input type="text" name="login" value="" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td>Hasło</td>
                        <td><input type="password" name="password" value="" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td>Powtórz Hasło</td>
                        <td><input type="password" name="repassword" value="" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td>Imie</td>
                        <td><input type="text" name="name" value="" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td>Nazwisko</td>
                        <td><input type="text" name="surname" value="" maxlength="30"/></td>
                    </tr>
                    <tr>
                        <td><input type="reset" value="Wyczyść" /></td>
                        <td><input type="submit" value="Rejestruj" /></td>
                    </tr>
                </tbody>
            </table>

        </form>
        <p><A HREF="LogIn.jsp">Powrót</A></p>
    </body>
</html>
