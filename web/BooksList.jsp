<%-- 
    Document   : BooksList
    Created on : 2015-03-22, 08:45:56
    Author     : mik
--%>

<%@page import="tai.beans.BookBeans"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<jsp:useBean id="sql" scope="session" class="tai.beans.UseSqlLiteDataBaseBeans" />

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista książek</title>
    </head>
    <body>
        <jsp:include page="LogStatus.jsp" />
        <%  ArrayList<BookBeans> books;
            books = sql.selectBookRecord(request.getParameter("choose"), request.getParameter("phase"));
        %>
        <h2>

            <form action="BooksList.jsp">
                <table border="1">
                    <thead>
                        <tr>
                            <th><select name="choose">
                                    <option></option>
                                    <option>Autor</option>
                                    <option>Tytuł</option>
                                    <option>Opis</option>
                                </select></th>
                            <th><input type="text" name="phase" value="" /></th>
                            <th><input type="submit" value="Wyświetl" /></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </form>

            <table border="1">
                <thead>
                    <tr>
                        <th>Autor</th>
                        <th>Tytuł</th>
                        <th>Szczegóły</th>
                    </tr>
                </thead>
                <tbody>
                    <%  for (BookBeans book : books) {%>
                    <tr>
                        <td><%=book.getAuthor()%></td>
                        <td><%=book.getTitle()%></td>
                        <td>
                            <form action="BookDetails.jsp" method="POST">
                                <input type="hidden" name="title" value="<%=book.getTitle()%>" />
                                <input type="submit" value="Szczegóły" />
                            </form>
                            <form action="UpdateBook.do">
                                <input type="hidden" name="action" value="delete" />
                                <input type="hidden" name="title" value="<%=book.getTitle()%>" />
                                <input type="submit" value="Usuń" />
                            </form>

                        </td>
                    </tr>
                    <%}%>
                </tbody>
            </table>

        </h2> 
        <p><A HREF="Panel.jsp">Panel Głowny</A></p>
    </body>
</html>
