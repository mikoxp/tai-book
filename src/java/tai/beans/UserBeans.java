package tai.beans;

import java.io.Serializable;

/**
 *
 * @author mik
 */
public class UserBeans implements Serializable{
    private String login;
    private String password;
    private String name;
    private String surname;
    private String level;

    public UserBeans() {
    }
/**
 * 
 * @param login login
 * @param password hasło
 * @param name imie
 * @param surname nazwisko
 * @param level  poziom uprawnien
 */
    public UserBeans(String login, String password, String name, String surname, String level) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.level = level;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
    public boolean IsAdmin()
    {
        if(level.equals("admin"))
            return true;
        return false;
    }
    
}
