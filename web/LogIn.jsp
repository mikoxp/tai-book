<%-- 
    Document   : LogIn
    Created on : 2015-03-27, 15:35:15
    Author     : mik
--%>
<% request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
    session.invalidate();
    request.getSession();
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Strona Logowania</title>
    </head>
    <body>
        <form action="VerificationServlet.do">
            <table border="1" cellpadding="1">

                <tbody>
                    <tr>
                        <td>Login</td>
                        <td><input type="text" name="login" value="" /></td>
                    </tr>
                    <tr>
                        <td>Hasło</td>
                        <td><input type="password" name="password" value="" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="Zaloguj" /></td>
                    </tr>
                </tbody>
            </table>
        </form>

        <form action="UserForm.jsp">
            <table border="0">
                <thead>
                    <tr>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td><input type="submit" value="Rejestracja" /></td>
                    </tr>
                </tbody>
            </table>

        </form>
    </body>
</html>
